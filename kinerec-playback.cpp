#include <stdexcept>
#include <fstream>
#include <thread>
#include <atomic>

#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/packet_pipeline.h>

#define KINEREC_VERSION "kinerec 1.0"

namespace libfreenect2
{

  struct ColorCameraParams
  {
    float fx;
    float fy;
    float cx;
    float cy;

    float shift_d, shift_m;

    float mx_x3y0;
    float mx_x0y3;
    float mx_x2y1;
    float mx_x1y2;
    float mx_x2y0;
    float mx_x0y2;
    float mx_x1y1;
    float mx_x1y0;
    float mx_x0y1;
    float mx_x0y0;

    float my_x3y0;
    float my_x0y3;
    float my_x2y1;
    float my_x1y2;
    float my_x2y0;
    float my_x0y2;
    float my_x1y1;
    float my_x1y0;
    float my_x0y1;
    float my_x0y0;
  };

  struct IrCameraParams
  {
    float fx;
    float fy;
    float cx;
    float cy;
    float k1;
    float k2;
    float k3;
    float p1;
    float p2;
  };

  struct Config
  {
    float MinDepth;
    float MaxDepth;

    bool EnableBilateralFilter;
    bool EnableEdgeAwareFilter;
  };

struct Freenect2Device
{
    Freenect2Device(std::string const& path);
    virtual ~Freenect2Device();
    virtual std::string getSerialNumber();
    virtual std::string getFirmwareVersion();
    virtual ColorCameraParams getColorCameraParams();
    virtual IrCameraParams getIrCameraParams();
    virtual void setColorCameraParams(ColorCameraParams const& cc);
    virtual void setIrCameraParams(IrCameraParams const& ic);
    virtual void setConfiguration(Config const& cf);
    virtual void setColorFrameListener(FrameListener *fl);
    virtual void setIrAndDepthFrameListener(FrameListener *fl);
    virtual bool start();
    virtual bool stop();
    virtual bool close();
    virtual bool startStreams(bool rgb, bool d);

    std::string p; ColorCameraParams c; IrCameraParams i;
    FrameListener *cf, *df;
    std::atomic<bool> flg;
    std::thread t;
};

Freenect2Device::Freenect2Device(std::string const& path) : p(path), flg(false)
{
    std::ifstream f(p, std::ios_base::in | std::ios_base::binary);
    f.read((char*)&c, sizeof(c));
    f.read((char*)&i, sizeof(i));
    f.close();
}
Freenect2Device::~Freenect2Device() {}
std::string Freenect2Device::getSerialNumber() { return "kinerec"; }
std::string Freenect2Device::getFirmwareVersion() { return KINEREC_VERSION; }
ColorCameraParams Freenect2Device::getColorCameraParams() { return c; }
IrCameraParams Freenect2Device::getIrCameraParams() { return i; }
void Freenect2Device::setColorCameraParams(ColorCameraParams const& cc) { throw std::logic_error("Unsupported"); }
void Freenect2Device::setIrCameraParams(IrCameraParams const& ic) { throw std::logic_error("Unsupported"); }
void Freenect2Device::setConfiguration(Config const& cf) { throw std::logic_error("Unsupported"); }
void Freenect2Device::setColorFrameListener(FrameListener *fl) { cf = fl; }
void Freenect2Device::setIrAndDepthFrameListener(FrameListener *fl) { df = fl; }
bool Freenect2Device::start() { flg = true; t = std::thread([this](){
    std::ifstream f(p, std::ios_base::in | std::ios_base::binary);
    f.seekg(sizeof(ColorCameraParams) + sizeof(IrCameraParams));
    while (flg.load()) {
        if (f) {
            Frame *rgb = new Frame(1920, 1080, 4);
            Frame *ir  = new Frame(512, 424, 4);
            Frame *depth = new Frame(512, 424, 4);
            f.read((char*) rgb->data, 1920*1080*4);
            f.read((char*) ir->data, 512*424*4);
            f.read((char*)depth->data, 512*424*4);
            if (!cf->onNewFrame(Frame::Type::Color, rgb)) delete rgb;
            if (!df->onNewFrame(Frame::Type::Ir, ir)) delete ir;
            if (!df->onNewFrame(Frame::Type::Depth, depth)) delete depth;
        } else f.seekg(sizeof(ColorCameraParams) + sizeof(IrCameraParams));
    }
}); return true; }
bool Freenect2Device::stop() { flg = false; t.join(); return true; }
bool Freenect2Device::close() { return true; }
bool Freenect2Device::startStreams(bool rgb, bool d) { if (!rgb || !d) throw std::logic_error("Unsupported"); return start(); }

struct Freenect2
{
    Freenect2(void* usb_context=nullptr);
    int enumerateDevices();
    std::string getDeviceSerialNumber(int);
    std::string getDefaultDeviceSerialNumber();
    Freenect2Device *openDevice(int);
    Freenect2Device *openDevice(int, const PacketPipeline* p);
    Freenect2Device *openDevice(const std::string&);
    Freenect2Device *openDevice(const std::string&, const PacketPipeline* p);
    Freenect2Device *openDefaultDevice();
    Freenect2Device *openDefaultDevice(const PacketPipeline* p);
    
    std::string m_path;
};

Freenect2::Freenect2(void* usb_context)
{
    auto envv = getenv("KINEREC_DATA");
    if (envv) m_path = envv;
    else throw std::logic_error("You must specify KINEREC_DATA before launching kinerec's playback features");
}

int Freenect2::enumerateDevices() { return 1; }
std::string Freenect2::getDeviceSerialNumber(int) { return "kinerec"; }
std::string Freenect2::getDefaultDeviceSerialNumber() { return "kinerec"; }
Freenect2Device *Freenect2::openDevice(int) { return new Freenect2Device(m_path); }
Freenect2Device *Freenect2::openDevice(int, const PacketPipeline* p) { delete p; return new Freenect2Device(m_path); }
Freenect2Device *Freenect2::openDevice(const std::string&) { return new Freenect2Device(m_path); }
Freenect2Device *Freenect2::openDevice(const std::string&, const PacketPipeline* p) { delete p; return new Freenect2Device(m_path); }
Freenect2Device *Freenect2::openDefaultDevice() { return new Freenect2Device(m_path); }
Freenect2Device *Freenect2::openDefaultDevice(const PacketPipeline* p) { delete p; return new Freenect2Device(m_path); }

}
