all:
	g++ -std=c++1z -O3 -march=native -g -Wall -Wextra -pedantic kinerec-record.cpp -lfreenect2 -lOpenCL -lpthread -o kinerec-record
	g++ -std=c++1z -O3 -march=native -Wall -Wextra -pedantic -fPIC -rdynamic -shared kinerec-playback.cpp -o kinerec-playback.so
